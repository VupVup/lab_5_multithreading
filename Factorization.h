#ifndef FACTORIZATION_H
#define FACTORIZATION_H
#include <vector>
#include <string>
#include "FactorizationException.h"

using namespace std;

class Factorization {
public:
	Factorization(uint64_t num);

	~Factorization();

	string pprint();

private:
	void factor(uint64_t num, uint64_t start);

	uint64_t num;
	vector<uint64_t> factors;
};
#endif
