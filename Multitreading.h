#pragma once

#include <iostream>
#include <fstream>
#include <string>
#include <queue>
#include <mutex>
#include <thread>
#include <condition_variable>
#include "Factorization.h"

class Multithreading {
public:
	Multithreading(char* input, char* output, int threadNum);

	~Multithreading();

private:
	void writefile(char* output);

	void getcommand();

	void factnum();

	void readfile(char* input, char* output);

	mutex mCout, mPause, mLocktasks, mLockpprint, mTaskLen, mPrintLen;
	string mode = "resume";
	condition_variable condNumRead, condPause, condWrite, condTaskLen, condPrintLen;
	bool done = false;
	queue<uint64_t> tasks;
	queue<string> pprints;
};