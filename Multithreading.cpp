#include "Multitreading.h"

using namespace std;

Multithreading::Multithreading(char* input, char* output, int threadNum) {
	if (!threadNum || threadNum > 8 || threadNum < 1)
		threadNum = 1;
	vector<thread> consumers;
	for (int i = 0; i != threadNum; i++)
		consumers.push_back(thread(&Multithreading::factnum, this));
	thread producer(&Multithreading::readfile, this, input, output);
	producer.detach();
	for (auto &thr : consumers) {
		// cout << "JOINING " << t.get_id() << endl;
		thr.join();
		// cout << "JOINED " << t.get_id() << endl;
	}
}

Multithreading::~Multithreading() {}

void Multithreading::getcommand() {
	string prev = mode;
	while (mode != "exit") {
		cout << "Enter the mode: pause, resume or exit\n";
		prev = mode;
		cin >> mode;
		if (done)
			mode = "exit";
		if (mode == "resume" || prev == "pause" && mode == "exit")
			condPause.notify_all();
	}
}

void Multithreading::writefile(char* output) {
	ofstream f(output);
	while (!done) {
		unique_lock<mutex> lockwrite(mLockpprint);

		while (this->pprints.empty() && mode != "exit")
			condWrite.wait(lockwrite);

		if (mode == "pause") {
			f.close();
			unique_lock<mutex> lockpause(mPause);
			condPause.wait(lockpause);
			f.open(output, ios::app);
		}

		while (!this->pprints.empty()) {
			f << this->pprints.front();
			this->pprints.pop();
			if (this->pprints.size() == 999)
				condPrintLen.notify_all();
		}
	}
}

void Multithreading::factnum() {
	while (!done) {
		if (mode == "exit")
			break;

		if (mode == "pause") {
			unique_lock<mutex> pause(mPause);
			condPause.wait(pause);
		}

		unique_lock<mutex> locktasks(mLocktasks);

		while (this->tasks.empty() && mode != "exit")
			condNumRead.wait(locktasks);

		while (!this->tasks.empty()) {
			unique_lock<mutex> lockprint(mLockpprint);
			Factorization num(this->tasks.front());
			if (this->pprints.size() < 1000)
				this->pprints.emplace(num.pprint());
			else
				while (this->pprints.size() >= 1000) {
					unique_lock<mutex> locklen(mPrintLen);
					condPrintLen.wait(locklen);
				}
			// unique_lock<mutex> lockCout(mCout);
			// cout << "factorized " << this->tasks.front() << " by " << this_thread::get_id() << endl;
			this->tasks.pop();
			if (this->tasks.size() == 999)
				condTaskLen.notify_one();
			condWrite.notify_one();
		}
	}
	// unique_lock<mutex> lockCout(mCout);
	// cout << "DONE BY " << this_thread::get_id() << endl;
}

void Multithreading::readfile(char* input, char* output) {
	ifstream f(input);
	uint64_t num;
	thread commander(&Multithreading::getcommand, this);
	thread writer(&Multithreading::writefile, this, output);

	while (f >> num) {
		if (mode == "exit") {
			cout << "Program has been interrupted\n";
			done = true;
			commander.join();
			condWrite.notify_one();
			writer.join();
			break;
		}

		if (mode == "pause") {
			cout << "Program has been paused\n";
			unique_lock<mutex> lockpause(mPause);
			condPause.wait(lockpause);
			cout << "Program has been resumed\n";
		}

		 this_thread::sleep_for(chrono::seconds(1));
		unique_lock<mutex> locktasks(mLocktasks);
		if (this->tasks.size() < 1000)
			this->tasks.emplace(num);
		else
			while (this->tasks.size() >= 1000) {
				unique_lock<mutex> locklen(mTaskLen);
				condTaskLen.wait(locklen);
			}
		// unique_lock<mutex> lockCout(mCout);
		// cout << "new task: " << num << endl;
		condNumRead.notify_one();
	}

	done = true;
	mode = "exit";
	condNumRead.notify_all();
	if (writer.joinable())
		writer.join();
	if (commander.joinable())
		commander.join();
}