#pragma once

#include <exception>
#include <string>
#ifndef FACTORIZATIONEXCEPTION_H
#define FACTORIZATIONEXCEPTION_H

class FactorizationException : public std::exception
{
public:
	FactorizationException(const std::string &message);
	~FactorizationException() noexcept;

	const char* what();

private:
	std::string message;
};

class WrongNumber : public FactorizationException
{
public:
	WrongNumber(const std::string &message);
};

#endif
